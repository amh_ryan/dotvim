" Colors {{{
set background=dark
color railscasts
set gfn=Hack:h12
set guioptions=Ace
set transparency=0
" }}}

" Mac Specific Shortcuts {{{
if has("gui_macvim")
    macmenu &File.New\ Tab key=<nop>
    map <D-t> <Plug>PeepOpen
    map <C-p> <Plug>PeepOpen
end
map <D-F> :Ag<space>
" }}}
