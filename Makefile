all: install

install:
	rm  ~/.vimrc
	rm  ~/.gvimrc
	rm  ~/.config/nvim/init.vim
	cp  vimrc ~/.vimrc
	cp  gvimrc ~/.gvimrc
	cp  nvim-init ~/.config/nvim/init.vim
	vim +PluginInstall +qall

update:
	cp ~/.vimrc .
